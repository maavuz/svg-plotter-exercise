# svg-plotter-exercise

## Prerequisites
    - node
    - yarn
    - vue-cli

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```


### Preview

![Preview](https://media.giphy.com/media/fT1JJIgoh67pioNfN6/source.gif)


### App Details

It works and tested on both chrome(83.0.4103.11) and firefox(v77.0.1) and Uses Tailwindcss for styling. no other packages are used.